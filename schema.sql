CREATE TABLE IF NOT EXISTS `user` (
    `username` text,
    `user_id` date,
    `password_bcrypt_hash` text[]
);

CREATE TABLE IF NOT EXISTS `email` (
    `id` SERIAL PRIMARY KEY,
    `user_id` EXTERNAL KEY,
    `sent_at` TIMESTAMP NOT NULL,
    `from` TEXT,
    `to` TEXT,
    `body` TEXT,
);
