use chrono::DateTime;
use std::io;
use tokio::io::AsyncBufReadExt;
use tokio::io::AsyncWriteExt;
use tokio::io::BufReader;
use tokio::net::TcpListener;

#[tokio::main]
async fn main() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:25").await?;
    for connection in listener.accept().await {
        let (mut stream, _addr) = connection;
        let (stream_r, mut stream_w) = stream.split();
        stream_w.write(b"220 smtp.hashdos.io").await?;
        let lines = &mut BufReader::new(stream_r).lines();
        while let Some(line) = lines.next_line().await? {
            let smtp_command = SmtpCommand::parse(line.as_str()).unwrap();
            match smtp_command {
                SmtpCommand::Helo { domain } => {
                    stream_w.write(b"").await?;
                }
                SmtpCommand::Quit => {
                    stream_w.write(b"221 Bye\n").await?;
                    break;
                }
                _ => (),
            }
        }
    }
    Ok(())
}

struct Email {
    from: String,
    to: String,
    cc: Vec<String>,
    date: DateTime<chrono::FixedOffset>,
}

enum SmtpCommand {
    Helo { domain: String },
    Ehlo { domain: String },
    Mail { from: String },
    Rcpt { to: String },
    Data,
    Quit,
}

impl SmtpCommand {
    fn parse(s: &str) -> Option<Self> {
        let (cmd, _args) = s.split_once(' ').unwrap();
        Some(match cmd {
            "HELO" => Self::Helo {
                domain: String::new(),
            },
            "EHLO" => Self::Ehlo {
                domain: String::new(),
            },
            "MAIL" => Self::Mail {
                from: String::new(),
            },
            "DATA" => Self::Data,
            "QUIT" => Self::Quit,
            _ => return None,
        })
    }
}
