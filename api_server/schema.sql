CREATE TABLE IF NOT EXISTS `user` (
    `id` BIGINT NOT NULL PRIMARY KEY,
    `username` VARCHAR(64),
    `password_bcrypt_hash` TEXT
);

CREATE UNIQUE INDEX `user_by_username` ON `user` (`username`);

CREATE TABLE IF NOT EXISTS `email` (
    `id` BIGINT NOT NULL PRIMARY KEY,
    `sent_at` TIMESTAMP NOT NULL,
    `from` TEXT NOT NULL,
    `to` TEXT NOT NULL,
    `body` TEXT NOT NULL,
    `mailbox_id` BIGINT REFERENCES `mailbox` (`id`)
);

CREATE UNIQUE INDEX `email_by_mailbox` ON `email` (`mailbox_id`);

CREATE TABLE IF NOT EXISTS `mailbox` (
    `id` BIGINT NOT NULL PRIMARY KEY,
    `user_id` EXTERNAL KEY,
    `name` TIMESTAMP NOT NULL,
);
