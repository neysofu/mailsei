use std::io;
use tokio::io::AsyncBufReadExt;
use tokio::io::AsyncWriteExt;
use tokio::io::BufReader;
use tokio::net::TcpListener;

pub async fn imap_server() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:587").await?;
    for connection in listener.accept().await {
        let (mut stream, _addr) = connection;
        let (stream_r, mut stream_w) = stream.split();
        stream_w.write(b"OK IMAP4rev1 Service Ready\r\n").await?;
        let lines = &mut BufReader::new(stream_r).lines();
        while let Some(line) = lines.next_line().await? {
            let operation = ImapOperation::parse(line.as_str()).unwrap();
            match operation.details {
                ImapOperationDetails::Login { username, password } => {}
                ImapOperationDetails::Logout => {
                    stream_w
                        .write_all(b"* BYE IMAP4rev1 server terminating connection")
                        .await?;
                    stream_w.write_all(b"a006 OK LOGOUT completed").await?;
                }
                ImapOperationDetails::Create { mailbox } => {}
                _ => {}
            }
        }
    }
    Ok(())
}

pub struct ImapOperation {
    tag: String,
    details: ImapOperationDetails,
}

impl ImapOperation {
    pub fn tag(&self) -> &str {
        self.tag.as_str()
    }

    fn parse(s: &str) -> Option<Self> {
        let (tag, other) = s.split_once(' ')?;
        Some(Self {
            tag: tag.to_string(),
            details: ImapOperationDetails::parse(other)?,
        })
    }
}

enum ImapOperationDetails {
    Login {
        username: String,
        password: String,
    },
    Select {
        mailbox: String,
    },
    Fetch,
    Create {
        mailbox: String,
    },
    Delete {
        mailbox: String,
    },
    Rename {
        mailbox: String,
        new_name: String,
    },
    Subscribe {
        mailbox: String,
    },
    Unsubscribe {
        mailbox: String,
    },
    List {
        ref_name: String,
        mailbox_pattern: String,
    },
    Lsub {
        ref_name: String,
        mailbox_pattern: String,
    },
    Status {
        mailbox: String,
        items: Vec<String>,
    },
    Append {
        mailbox: String,
        flag: Option<String>,
        datetime: Option<String>,
        message: String,
    },
    Check,
    Close,
    Expunge,
    Search {
        charset: Option<String>,
        criteria: Vec<String>,
    },
    Store,
    Logout,
}

impl ImapOperationDetails {
    fn parse(s: &str) -> Option<Self> {
        let (cmd, _args) = s.split_once(' ')?;
        Some(match cmd {
            "login" => Self::Login {
                username: String::new(),
                password: String::new(),
            },
            "select" => Self::Select {
                mailbox: String::new(),
            },
            "create" => Self::Create {
                mailbox: String::new(),
            },
            "delete" => Self::Delete {
                mailbox: String::new(),
            },
            "rename" => Self::Rename {
                mailbox: String::new(),
                new_name: String::new(),
            },
            "subscribe" => Self::Subscribe {
                mailbox: String::new(),
            },
            _ => return None,
        })
    }
}
