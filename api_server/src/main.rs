use actix_web::{App, HttpServer};
use sqlx::postgres::PgPoolOptions;
use std::io;

mod config;
mod dal;
mod endpoints;
mod imap;

#[actix_web::main]
async fn main() -> io::Result<()> {
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect("postgres://postgres:password@localhost/test")
        .await
        .unwrap();
    HttpServer::new(|| {
        App::new()
            .service(endpoints::login)
            .service(endpoints::signup)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}

pub struct AppState {
    dal: dal::Dal,
}