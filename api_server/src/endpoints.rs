use super::AppState;
use actix_web::FromRequest;
use actix_web::{get, web};
use serde::Deserialize;

#[get("/login")]
async fn login(state: web::Data<AppState>, request: web::HttpRequest) -> String {
    String::new()
}

#[get("/signup")]
async fn signup(state: web::Data<AppState>, request: web::HttpRequest) -> String {
    let bytes = web::Bytes::extract(&request).await.unwrap();
    let form: SignupForm = serde_json::from_slice(&bytes[..]).unwrap();
    state
        .dal
        .signup(form.username.as_str(), form.password.as_str())
        .await
        .unwrap();
    String::new()
}

#[derive(Deserialize)]
struct SignupForm {
    username: String,
    password: String,
}

#[cfg(test)]
mod test {
    use super::*;
}
