pub struct Dal {
    pool: sqlx::Pool<sqlx::Postgres>,
}

impl Dal {
    pub async fn attempt_login(&self, username: &str, password: &str) -> sqlx::Result<bool> {
        let result: (String,) = sqlx::query_as(
            "
            SELECT `password`
            FROM `user`
            WHERE `username` == $1;
        ",
        )
        .bind(username)
        .fetch_one(&self.pool)
        .await?;
        Ok(password == result.0)
    }

    pub async fn signup(&self, username: &str, password: &str) -> sqlx::Result<()> {
        sqlx::query(
            "
            INSERT INTO `user`
            VALUES
            ($1, $2);
        ",
        )
        .bind(username)
        .bind(password)
        .execute(&self.pool)
        .await?;
        Ok(())
    }
}
