use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    db_name: String,
    db_password: String,
    db_ip: String,
    db_port: u16,
    db_path: String,
}

impl Config {
    pub fn db_name(&self) -> &str {
        self.db_name.as_str()
    }

    pub fn db_password(&self) -> &str {
        self.db_password.as_str()
    }

    pub fn db_ip(&self) -> &str {
        self.db_ip.as_str()
    }

    pub fn db_port(&self) -> u16 {
        self.db_port
    }

    pub fn db_path(&self) -> &str {
        self.db_path.as_str()
    }
}
